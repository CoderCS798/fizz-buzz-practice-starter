package tdd.fizzbuzz;

public class FizzBuzz {
    public String countOff(int normalCase) {
        if(normalCase%3==0&&normalCase%5!=0){
            return "Fizz";
        }
        if(normalCase%3!=0&&normalCase%5==0){
            return "Buzz";
        }
        if(normalCase%3==0&&normalCase%5==0){
            return "FizzBuzz";
        }
        return String.valueOf(normalCase);
    }
}
