package tdd.fizzbuzz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {
    @Test
    void should_return_givenNumber_when_countOff_given_normal_case(){
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        int normalCase = 1;
        //when
        String result =  fizzBuzz.countOff(normalCase);
        //then
        Assertions.assertEquals("1",result);
    }
    @Test
    void should_return_fizz_when_countOff_given_multiple_of_3(){
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        int normalCase = 9;
        //when
        String result =  fizzBuzz.countOff(normalCase);
        //then
        Assertions.assertEquals("Fizz",result);
    }
    @Test
    void should_return_buzz_when_countOff_given_multiple_of_5(){
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        int normalCase = 10;
        //when
        String result =  fizzBuzz.countOff(normalCase);
        //then
        Assertions.assertEquals("Buzz",result);
    }
    @Test
    void should_return_FizzBuzz_when_countOff_given_multiple_of_3_and_5(){
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        int normalCase = 15;
        //when
        String result =  fizzBuzz.countOff(normalCase);
        //then
        Assertions.assertEquals("FizzBuzz",result);
    }
    @Test
    void should_return_Whizz_when_countOff_given_multiple_of_7(){
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        int normalCase = 14;
        //when
        String result =  fizzBuzz.countOff(normalCase);
        //then
        Assertions.assertEquals("Whizz",result);
    }
}
